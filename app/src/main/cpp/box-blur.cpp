#include <jni.h>
#include <string>
#include <android/log.h>


extern "C" {

const int radius = 5;
const int pixels_on_row = 1 + radius * 2;

float BOX_5x5[] = {
        (float) 1 / 11,
        (float) 1 / 11,
        (float) 1 / 11,
        (float) 1 / 11,
        (float) 1 / 11,
        (float) 1 / 11,
        (float) 1 / 11,
        (float) 1 / 11,
        (float) 1 / 11,
        (float) 1 / 11,
        (float) 1 / 11,
};

struct arg_struct {
    int width;
    int height;
    int start;
    int end;
};

jint *src;
jint *dst;

void *vertical(void *arguments) {
    struct arg_struct *args = (struct arg_struct *) arguments;
    int height = args->height;
    int width = args->width;
    int start = args->start;
    int end = args->end;

    int offset;
    for (int y = 0; y < height; y++) {
        for (int x = start; x < end; x++) {
            int sumR = 0, sumG = 0, sumB = 0;
            offset = 0;
            for (int yoffset = -radius; yoffset <= radius; yoffset++) {
                int sy = std::max(0, std::min(height - 1, y + yoffset));
                int clr = dst[width * sy + x];
                sumR += ((clr >> 16) & 0xff) * BOX_5x5[offset];
                sumG += ((clr >> 8) & 0xff) * BOX_5x5[offset];
                sumB += (clr & 0xff) * BOX_5x5[offset];
                offset++;
            }

            // get final Red
            if (sumR < 0) {
                sumR = 0;
            } else if (sumR > 255) {
                sumR = 255;
            }

            // get final Green
            if (sumG < 0) {
                sumG = 0;
            } else if (sumG > 255) {
                sumG = 255;
            }

            // get final Blue
            if (sumB < 0) {
                sumB = 0;
            } else if (sumB > 255) {
                sumB = 255;
            }

            int sy = std::max(0, std::min(height - 1, y));
            dst[width * sy + x] = 0xff << 24 | ((sumR) << 16) | ((sumG) << 8) | (sumB);
        }
    }
    return NULL;
}

void *horizontal(void *arguments) {
    struct arg_struct *args = (struct arg_struct *) arguments;
    int height = args->height;
    int width = args->width;
    int start = args->start;
    int end = args->end;

    int offset;
    for (int y = start; y < end; y++) {
        int row = y * width;              ///< Specifies the current row
        for (int x = 0; x < width; x++) {
            int sumR = 0, sumG = 0, sumB = 0;
            offset = 0;
            for (int xoffset = -radius; xoffset <= radius; xoffset++) {

                int sx = std::max(0, std::min(width - 1, x + xoffset));
                int p = src[row + sx];
                sumR += ((p >> 16) & 0xff) * BOX_5x5[offset];
                sumG += ((p >> 8) & 0xff) * BOX_5x5[offset];
                sumB += (p & 0xff) * BOX_5x5[offset];
                offset++;

            }

            // get final Red
            if (sumR < 0) {
                sumR = 0;
            } else if (sumR > 255) {
                sumR = 255;
            }

            // get final Green
            if (sumG < 0) {
                sumG = 0;
            } else if (sumG > 255) {
                sumG = 255;
            }

            // get final Blue
            if (sumB < 0) {
                sumB = 0;
            } else if (sumB > 255) {
                sumB = 255;
            }

            int sx = std::max(0, std::min(width - 1, x));
            dst[row + sx] = 0xff << 24 | ((sumR) << 16) | ((sumG) << 8) | (sumB);

        }
    }
    return NULL;
}

JNIEXPORT void JNICALL
Java_se_apals_bitmapblur_bluralgorithms_box_BoxBlurCpp_boxBlur(JNIEnv *env,
                                                               jobject instance,
                                                               jintArray src_,
                                                               jintArray dst_,
                                                               jint width,
                                                               jint height) {
    jint *src = env->GetIntArrayElements(src_, NULL);
    jint *dst = env->GetIntArrayElements(dst_, NULL);

    int offset;
    int i = 0;
    for (int y = 0; y < height; y++) {
        int row = y * width;              ///< Specifies the current row
        for (int x = 0; x < width; x++) {
            int sumR = 0, sumG = 0, sumB = 0;
            offset = 0;
            for (int xoffset = -radius; xoffset <= radius; xoffset++) {

                int sx = std::max(0, std::min(width - 1, x + xoffset));
                jint p = src[row + sx];
                sumR += ((p >> 16) & 0xff) * BOX_5x5[offset];
                sumG += ((p >> 8) & 0xff) * BOX_5x5[offset];
                sumB += (p & 0xff) * BOX_5x5[offset];
                offset++;
            }

            // get final Red
            sumR = (int) (sumR / 1 + 0);
            if (sumR < 0) {
                sumR = 0;
            } else if (sumR > 255) {
                sumR = 255;
            }

            // get final Green
            sumG = (int) (sumG / 1 + 0);
            if (sumG < 0) {
                sumG = 0;
            } else if (sumG > 255) {
                sumG = 255;
            }

            // get final Blue
            sumB = (int) (sumB / 1 + 0);
            if (sumB < 0) {
                sumB = 0;
            } else if (sumB > 255) {
                sumB = 255;
            }

            // Set our calculated value to our temp map
            dst[i++] = 0xff << 24 | ((sumR) << 16) | ((sumG) << 8) | (sumB);

        }
    }

    i = 0;
    for (int y = 0; y < height; y++) {
        for (int x = 0; x < width; x++) {
            int sumR = 0, sumG = 0, sumB = 0;
            offset = 0;
            for (int yoffset = -radius; yoffset <= radius; yoffset++) {

                int sy = std::max(0, std::min(height - 1, y + yoffset));
                jint p = dst[width * sy + x];
                sumR += ((p >> 16) & 0xff) * BOX_5x5[offset];
                sumG += ((p >> 8) & 0xff) * BOX_5x5[offset];
                sumB += (p & 0xff) * BOX_5x5[offset];
                offset++;
            }

            // get final Red
            if (sumR < 0) {
                sumR = 0;
            } else if (sumR > 255) {
                sumR = 255;
            }

            // get final Green
            if (sumG < 0) {
                sumG = 0;
            } else if (sumG > 255) {
                sumG = 255;
            }

            // get final Blue
            if (sumB < 0) {
                sumB = 0;
            } else if (sumB > 255) {
                sumB = 255;
            }

            // Set our calculated value to our temp map
            dst[i++] = 0xff << 24 | ((sumR) << 16) | ((sumG) << 8) | (sumB);
        }
    }

    env->ReleaseIntArrayElements(src_, src, 0);
    env->ReleaseIntArrayElements(dst_, dst, 0);
}


JNIEXPORT void JNICALL
Java_se_apals_bitmapblur_bluralgorithms_box_BoxBlurThreadedCpp_boxBlur(JNIEnv *env,
                                                                       jobject instance,
                                                                       jintArray src_,
                                                                       jintArray dst_,
                                                                       jint width,
                                                                       jint height,
                                                                       jint availableProcessors) {
    src = env->GetIntArrayElements(src_, NULL);
    dst = env->GetIntArrayElements(dst_, NULL);

    pthread_t threads[availableProcessors];
    struct arg_struct args[availableProcessors];

    // HORIZONTAL PASS
    for (int a = 0; a < availableProcessors; a++) {
        pthread_t thread;
        // Used for calculating start and ending points
        args[a].width = width;
        args[a].height = height;
        args[a].start = a * height / availableProcessors;
        args[a].end = (a + 1) * height / availableProcessors;

        if (pthread_create(&thread, NULL, horizontal, &args[a])) {
            fprintf(stderr, "Error creating thread\n");
        }
        threads[a] = thread;
    }

    for (int a = 0; a < availableProcessors; a++) {
        pthread_join(threads[a], NULL);
    }

    // VERTICAL PASS
    for (int a = 0; a < availableProcessors; a++) {
        pthread_t thread;
        args[a].width = width;
        args[a].height = height;
        args[a].start = a * width / availableProcessors;
        args[a].end = (a + 1) * width / availableProcessors;

        if (pthread_create(&thread, NULL, vertical, &args[a])) {
            fprintf(stderr, "Error creating thread\n");
        }
        threads[a] = thread;
    }

    for (int a = 0; a < availableProcessors; a++) {
        pthread_join(threads[a], NULL);
    }


    env->ReleaseIntArrayElements(src_, src, 0);
    env->ReleaseIntArrayElements(dst_, dst, 0);
}


}