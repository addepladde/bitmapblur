#include <jni.h>
#include <string>
#include <android/log.h>
#include <algorithm>

extern "C" {

const int radius = 5;
const int pixels_on_row = 1 + radius * 2;

struct arg_struct {
    int width;
    int height;
    int start;
    int end;
};

jint *src;
jint *dst;


JNIEXPORT void JNICALL
Java_se_apals_bitmapblur_bluralgorithms_median_MedianBlurCpp_medianBlur(JNIEnv *env,
                                                                        jobject instance,
                                                                        jintArray src_,
                                                                        jintArray dst_,
                                                                        jint width,
                                                                        jint height) {
    jint *src = env->GetIntArrayElements(src_, NULL);
    jint *dst = env->GetIntArrayElements(dst_, NULL);



    /** Median Filter operation */
    for (int y = 0; y < height; y++) {
        for (int x = 0; x < width; x++) {
            int clr[radius * radius];
            int count = 0;
            for (int r = y - (radius / 2); r <= y + (radius / 2); r++) {
                for (int c = x - (radius / 2); c <= x + (radius / 2); c++) {
                    if (r >= 0 && r < height && c >= 0 && c < width) {
                        int px = src[r * width + c];
                        clr[count] = px;
                        count++;
                    }
                }
            }

//            __android_log_print(ANDROID_LOG_DEBUG, "JNI", "gauss, %d", clr[0]);

            std::sort(clr, clr + count);

//            std::nth_element(clr, clr + count / 2, clr + count);
//            median = v[len / 2];

            /** save median value in outputPixels array */
            int index = (count % 2 == 0) ? count / 2 - 1 : count / 2;
//                dstpixels[x + y * width] = 0xff << 24 | ((red[index]) << 16) | ((green[index]) << 8) | (blue[index]);
            dst[x + y * width] = clr[index];


        }
    }

    env->ReleaseIntArrayElements(src_, src, 0);
    env->ReleaseIntArrayElements(dst_, dst, 0);
}


void *medianBlur(void *arguments) {
    struct arg_struct *args = (struct arg_struct *) arguments;

    int start = args->start;
    int end = args->end;
    int width = args->width;
    int height = args->height;
    int clr[radius * radius];
    for (int y = start; y < end; y++) {
        for (int x = 0; x < width; x++) {
            int count = 0;
            for (int r = y - (radius / 2); r <= y + (radius / 2); r++) {
                for (int c = x - (radius / 2); c <= x + (radius / 2); c++) {
                    if (r >= 0 && r < height && c >= 0 && c < width) {
                        int px = src[r * width + c];
                        clr[count] = px;
                        count++;
                    }
                }
            }

            std::sort(clr, clr + count);

            /** save median value in outputPixels array */
            int index = (count % 2 == 0) ? count / 2 - 1 : count / 2;
            dst[x + y * width] = clr[index];
        }
    }

    return NULL;
}

JNIEXPORT void JNICALL
Java_se_apals_bitmapblur_bluralgorithms_median_MedianBlurThreadedCpp_medianBlur(JNIEnv *env,
                                                                                jobject instance,
                                                                                jintArray src_,
                                                                                jintArray dst_,
                                                                                jint width,
                                                                                jint height,
                                                                                jint availableProcessors) {
    src = env->GetIntArrayElements(src_, NULL);
    dst = env->GetIntArrayElements(dst_, NULL);

    pthread_t threads[availableProcessors];
    struct arg_struct args[availableProcessors];

    for (int a = 0; a < availableProcessors; a++) {
        pthread_t thread;
        // Used for calculating start and ending points
        args[a].width = width;
        args[a].height = height;
        args[a].start = a * height / availableProcessors;
        args[a].end = (a + 1) * height / availableProcessors;

        if (pthread_create(&thread, NULL, medianBlur, &args[a])) {
            fprintf(stderr, "Error creating thread\n");
        }
        threads[a] = thread;
    }

    for (int a = 0; a < availableProcessors; a++) {
        pthread_join(threads[a], NULL);
    }


    env->ReleaseIntArrayElements(src_, src, 0);
    env->ReleaseIntArrayElements(dst_, dst, 0);
}


}