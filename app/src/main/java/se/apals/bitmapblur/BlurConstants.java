package se.apals.bitmapblur;

/**
 * Created by apals on 2017-03-07.
 */

public class BlurConstants {

    public static final int RADIUS = 5;
    //
    public static float[] GAUSSIAN_5x5 = new float[]{
            0.000003f,
            0.000229f,
            0.005977f,
            0.060598f,
            0.24173f,
            0.382925f,
            0.24173f,
            0.060598f,
            0.005977f,
            0.000229f,
            0.000003f
    };

    public static float[] BOX_5x5 = new float[]{
            1f/11,
            1f/11,
            1f/11,
            1f/11,
            1f/11,
            1f/11,
            1f/11,
            1f/11,
            1f/11,
            1f/11,
            1f/11,
            1f/11,
    };

    //
    public static final float[] GAUSSIAN_5x5_FROM_OPENCV = new float[]{
            1.486719524872936e-06f,
            0.0001338302266775365f,
            0.00443184844216086f,
            0.05399096688137778f,
            0.2419707261692552f,
            0.3989422831220072f,
            0.2419707261692552f,
            0.05399096688137778f,
            0.00443184844216086f,
            0.0001338302266775365f,
            1.486719524872936e-06f};
}
