package se.apals.bitmapblur;

import android.graphics.Bitmap;

/**
 * Created by apals on 2017-03-06.
 */

public interface Blurrer {

    public String getName();

    public long blur(Bitmap src, Bitmap dst);
}
