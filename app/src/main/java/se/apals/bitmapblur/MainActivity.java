package se.apals.bitmapblur;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.SystemClock;
import android.renderscript.RenderScript;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import se.apals.bitmapblur.bluralgorithms.gammacorrection.GammaCorrection;
import se.apals.bitmapblur.bluralgorithms.greyscale.GreyScale;
import se.apals.bitmapblur.bluralgorithms.greyscale.GreyScaleOpenCv;
import se.apals.bitmapblur.bluralgorithms.greyscale.GreyScaleRelaxed;
import se.apals.bitmapblur.bluralgorithms.greyscale.GreyScaleThreadedCpp;
import se.apals.bitmapblur.bluralgorithms.greyscale.GreyScaleThreadedJava;
import se.apals.bitmapblur.bluralgorithms.thresholding.ThresholdingOpenCv;
import se.apals.bitmapblur.bluralgorithms.thresholding.ThresholdingRs;
import se.apals.bitmapblur.bluralgorithms.thresholding.ThresholdingRsRelaxed;
import se.apals.bitmapblur.bluralgorithms.thresholding.ThresholdingThreadedCpp;
import se.apals.bitmapblur.bluralgorithms.thresholding.ThresholdingThreadedJava;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = MainActivity.class.getSimpleName();
    private int mFinishedAlgorithms = 0;

    private ImageView mImageView;
    private LinearLayout mImagesContainer;


    private List<Blurrer> blurrers = new ArrayList<>(NUMBER_OF_ALGORITHMS);


    private static final int NUMBER_OF_ALGORITHMS = 1;
    private int[] bitmaps = new int[]{R.drawable.lena}; //R.drawable.obamasmall, R.drawable.obamamedium, R.drawable.obamahd};
    final int NUMBER_OF_RUNS = NUMBER_OF_ALGORITHMS * bitmaps.length;

    private ImageView[] results = new ImageView[NUMBER_OF_RUNS];
    private TextView[] labelAndTime = new TextView[NUMBER_OF_RUNS];
    private long[] setupTimes = new long[NUMBER_OF_RUNS];

    private RenderScript rs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        int numThreads = 4;
        long start = SystemClock.elapsedRealtimeNanos();
        Thread[] threads = new Thread[numThreads];
        for (int i = 0; i < numThreads; i++) {
            Thread t = new Thread(new Runnable() {
                @Override
                public void run() {
                }
            }, "Thread " + i);
            threads[i] = t;
            t.start();
        }
        for (int i = 0; i < numThreads; i++) {
            try {
                threads[i].join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        long end = SystemClock.elapsedRealtimeNanos();
        Log.e(TAG, "Creating " + numThreads + " threads took " + TimeUnit.NANOSECONDS.toMillis(end - start) + " ms");

        mImageView = (ImageView) findViewById(R.id.image);
        mImagesContainer = (LinearLayout) findViewById(R.id.images_container);
        rs = RenderScript.create(this);

        instantiateBlurrers();
        createLayoutsForBlurrers();

    }

    private void createLayoutsForBlurrers() {
        for (int i = 0; i < NUMBER_OF_ALGORITHMS; i++) {
            Blurrer blurrer = blurrers.get(i);
            for (int j = 0; j < bitmaps.length; j++) {
                LinearLayout a = new LinearLayout(this);

                a.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                a.setOrientation(LinearLayout.VERTICAL);
                TextView tv = new TextView(this);
                tv.setText(blurrer.getName());

                labelAndTime[i + j] = tv;


                ImageView iv = new ImageView(this);
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(200, 133);
                iv.setLayoutParams(layoutParams);
                results[i + j] = iv;
                iv.setBackgroundColor(Color.RED);

                a.addView(tv);
                a.addView(iv);
                mImagesContainer.addView(a);
            }
        }
    }

    private void instantiateBlurrers() {

        blurrers.add(new GammaCorrection());
//        blurrers.add(new ThresholdingThreadedJava());
//        blurrers.add(new ThresholdingRsRelaxed(rs));
//        blurrers.add(new ThresholdingRs(rs));
//        blurrers.add(new ThresholdingOpenCv());
//        blurrers.add(new ThresholdingThreadedCpp());
//
//
//        blurrers.add(new GreyScaleThreadedJava());
//        blurrers.add(new GreyScaleThreadedCpp());
//        blurrers.add(new GreyScaleRelaxed(rs));
//        blurrers.add(new GreyScale(rs));
//        blurrers.add(new GreyScaleOpenCv());
//        blurrers.add(new BoxBlur());
//        blurrers.add(new BoxBlurThreaded());
//        blurrers.add(new BoxBlurCpp());
//        blurrers.add(new BoxBlurThreadedCpp());
//        blurrers.add(new BoxBlurOpenCv());
//        blurrers.add(new BoxBlurRs(rs));
//        blurrers.add(new GreyScale(rs));
//        blurrers.add(new GaussianBlur());
//        blurrers.add(new GaussianBlurThreaded());
//        blurrers.add(new GaussianBlurCpp());
//        blurrers.add(new GaussianBlurThreadedCpp());
//        blurrers.add(new GaussianBlurRs(rs));
//        blurrers.add(new GaussianBlurIntrinsicRs(rs));
//        blurrers.add(new GaussianBlurOpenCv());
//        blurrers.add(new MedianBlurCpp());
//        blurrers.add(new MedianBlurThreadedCpp());
//        blurrers.add(new MedianBlur());
//        blurrers.add(new MedianBlurThreaded());
//        blurrers.add(new MedianBlurOpenCv());

    }

    int height, width;

    public void blur(View view) {
        AsyncTask<Void, Void, Void>[] tasks = new AsyncTask[NUMBER_OF_RUNS];
        Bitmap src = BitmapFactory.decodeResource(getResources(), bitmaps[0]);
        height = src.getHeight();
        width = src.getWidth();
//        Bitmap src2 = BitmapFactory.decodeResource(getResources(), bitmaps[1]);
//        Bitmap src3 = BitmapFactory.decodeResource(getResources(), bitmaps[2]);
        Bitmap dst = Bitmap.createBitmap(width, height, src.getConfig());


        Bitmap[] srcs = new Bitmap[]{src};//, src2, src3};
        for (int i = 0; i < NUMBER_OF_ALGORITHMS; i++) {

            for (int j = 0; j < bitmaps.length; j++) {
                tasks[i * bitmaps.length + j] = new BlurTask(i, j, srcs[j], dst);
            }
        }

        for (int i = 0; i < tasks.length; i++) {
            tasks[i].execute();
        }

    }

    private void done() {
        if (++mFinishedAlgorithms != NUMBER_OF_RUNS) return;

        for (int i = 0; i < NUMBER_OF_ALGORITHMS; i++) {
            for (int j = 0; j < i; j++) {
                String cls = blurrers.get(i).getClass().getSimpleName();
                String cls2 = blurrers.get(j).getClass().getSimpleName();
                if (cls.substring(0, 3).equals(cls2.substring(0, 3))) {
                    Bitmap one = ((BitmapDrawable) results[i].getDrawable()).getBitmap();
                    Bitmap two = ((BitmapDrawable) results[j].getDrawable()).getBitmap();
                    if (cls.endsWith("Blur") || cls2.endsWith("Blur")) {
                        calculateDifferences(cls, cls2, one, two);
                    }
                }

            }

        }

        writeToFile("-----------------------");
        Log.d(TAG, "DONE");

//        ((BitmapDrawable) results[4].getDrawable()).getBitmap().getPixels(pixels, 0, width, 0, 0, width, height);
//        ((BitmapDrawable) results[6].getDrawable()).getBitmap().getPixels(pixels2, 0, width, 0, 0, width, height);

//        int pixelsDiff = 0;
//        for (int k = 0; k < pixels.length; k++) {
//            String a = "(" + Color.red(pixels[k]) + ", " + Color.green(pixels[k]) + ", " + Color.blue(pixels[k]) + ")";
//            String b = "(" + Color.red(pixels2[k]) + ", " + Color.green(pixels2[k]) + ", " + Color.blue(pixels2[k]) + ")";
//            if (!a.equals(b)) {
//                Log.d(TAG, a  + " : " + b);
//            }
//        }
    }

    private void calculateDifferences(String cls, String cls2, Bitmap one, Bitmap two) {
        int[] pixels = new int[height * width];
        int[] pixels2 = new int[height * width];
        one.getPixels(pixels, 0, one.getWidth(), 0, 0, one.getWidth(), one.getHeight());
        two.getPixels(pixels2, 0, two.getWidth(), 0, 0, two.getWidth(), two.getHeight());
        double sum = 0;
        double sum1 = 0;
        double sum2 = 0;
        int max = -1;
        int rowMax = -1;
        int columnMax = -1;
        int[] f = new int[3];
        int[] s = new int[3];
        for (int i = 0; i < pixels.length; i++) {
            int diffThisPixel = Math.abs(Color.red(pixels2[i]) - Color.red(pixels[i])) +
                    Math.abs(Color.green(pixels2[i]) - Color.green(pixels[i])) +
                    Math.abs(Color.blue(pixels2[i]) - Color.blue(pixels[i]));
            sum += diffThisPixel;
            sum1 += Color.red(pixels[i]) + Color.green(pixels[i]) + Color.blue(pixels[i]);
            sum2 += Color.red(pixels2[i]) + Color.green(pixels2[i]) + Color.blue(pixels2[i]);

            if (diffThisPixel > max) {
                max = diffThisPixel;
                rowMax = i / width;
                columnMax = i - (rowMax * width);
                f[0] = Color.red(pixels[i]);
                f[1] = Color.green(pixels[i]);
                f[2] = Color.blue(pixels[i]);
                s[0] = Color.red(pixels2[i]);
                s[1] = Color.green(pixels2[i]);
                s[2] = Color.blue(pixels2[i]);
            }

        }
        Log.d(TAG, "Difference between " + cls + "(" + sum1 + ") and " + cls2 + "(" + sum2 + "): " + sum + ", max diff: " + max + " at " + columnMax + ", " + rowMax);
        Log.d(TAG, "Diff percent" + cls + "(" + (100f * (sum / sum1)) + "%) and " + cls2 + "(" + (100f * (sum / sum2)) + ")");
        Log.d(TAG, "avgdiff : " + (sum / (width * height)));
        Log.d(TAG, f[0] + ", " + f[1] + ", " + f[2]);
        Log.d(TAG, s[0] + ", " + s[1] + ", " + s[2]);
        Log.d(TAG, "------------------");
    }

    private class BlurTask extends AsyncTask<Void, Void, Void> {

        private final int bitmapindex;
        private int mIndex;
        private Bitmap src;
        private Bitmap dst;

        private long time;
        private long totalTime = 0;
        private long start, end, min = Long.MAX_VALUE, max = Long.MIN_VALUE;
        private long setupTime = 0;

        private Blurrer blurrer;

        final int WARMUP_ROUNDS = 10;
        final int TEST_ROUNDS = 30;
        final long[] times = new long[TEST_ROUNDS];

        public BlurTask(int i, int bitmapindex, Bitmap src, Bitmap dst) {
            this.blurrer = blurrers.get(i);
            this.bitmapindex = bitmapindex;
            this.mIndex = i;
            this.src = src;
            this.dst = dst;
        }

        @Override
        protected Void doInBackground(Void... voids) {


            for (int i = 0; i < WARMUP_ROUNDS; i++) {
                System.gc();
                blurrer.blur(src, dst);
            }

            for (int i = 0; i < TEST_ROUNDS; i++) {
                System.gc();
                start = SystemClock.elapsedRealtimeNanos();
                setupTime += blurrer.blur(src, dst);
                end = SystemClock.elapsedRealtimeNanos();

                time = end - start;
                times[i] = TimeUnit.NANOSECONDS.toMillis(time);
                if (time < min) {
                    min = time;
                } else if (time > max) {
                    max = time;
                }
                totalTime += time;
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            results[mIndex].setImageBitmap(dst);

            long avg = TimeUnit.NANOSECONDS.toMillis(totalTime / TEST_ROUNDS);
            long maxx = TimeUnit.NANOSECONDS.toMillis(max);
            long minn = TimeUnit.NANOSECONDS.toMillis(min);

            Log.d(TAG, "------------------------");
            Log.d(TAG, blurrer.getName());
            Log.d(TAG, "(" + (src.getWidth() * src.getHeight()) + ", " + avg + ")");
            Log.d(TAG, "avg: " + avg + ", min: " + minn + ", max: " + maxx + " ms, memoryUsed: " + (setupTime / TEST_ROUNDS) + "\n");
            StringBuilder s = new StringBuilder();
            s.append("[");

            for (long l : times) {
                s.append(l);
                s.append(", ");
            }
            s.append("]\n");
            Log.d(TAG, "times: " + s.toString());


            setupTimes[mIndex] = (int) (setupTime / TEST_ROUNDS);
            String report = blurrer.getName() + ", avg: " + avg + ", min: " + minn + ", max: " + maxx + " ms, setupTime: " + (setupTime / TEST_ROUNDS) + "\n";
            labelAndTime[mIndex].setText(report);
            done();


            writeToFile(report);
        }
    }

    private void writeToFile(String report) {
        String filename = "report.txt";
        FileOutputStream outputStream;

        try {
            outputStream = openFileOutput(filename, Context.MODE_PRIVATE);
            outputStream.write(report.getBytes());
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
