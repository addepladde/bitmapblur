package se.apals.bitmapblur.bluralgorithms.box;

import android.graphics.Bitmap;
import android.os.SystemClock;

import org.opencv.android.Utils;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;

import java.util.concurrent.TimeUnit;

import se.apals.bitmapblur.BlurConstants;
import se.apals.bitmapblur.Blurrer;
import se.apals.bitmapblur.MemoryUtils;

import static se.apals.bitmapblur.BlurConstants.BOX_5x5;
import static se.apals.bitmapblur.BlurConstants.GAUSSIAN_5x5;

/**
 * Created by apals on 2017-03-06.
 */

public class BoxBlurOpenCv implements Blurrer {

    final int radius = BlurConstants.RADIUS;
    final Mat kernel = new Mat(1, GAUSSIAN_5x5.length, CvType.CV_32FC1);

    static {
        System.loadLibrary("opencv_java3");
    }

    public BoxBlurOpenCv() {
        kernel.put(0, 0, BOX_5x5);
    }

    @Override
    public String getName() {
        return "Box Blur OpenCv Cpp with radius " + radius;
    }

    @Override
    public long blur(Bitmap src, Bitmap dst) {
        long memoryUsedBefore = MemoryUtils.getUsedMemorySize();

        long start = SystemClock.elapsedRealtimeNanos();
        Mat srcMat = new Mat();
        Mat dstMat = new Mat();

        Utils.bitmapToMat(src, srcMat);

        long end = SystemClock.elapsedRealtimeNanos();
        int setupTimeMs = (int) TimeUnit.NANOSECONDS.toMillis(end - start);
        // -1 indicates that we want the same depth as the source image
        Imgproc.filter2D(srcMat, dstMat, -1, kernel);
//        Log.d("GaussianBlurOpenCv", kernel.dump());


        Utils.matToBitmap(dstMat, dst);
        long memoryUsedAfter = MemoryUtils.getUsedMemorySize(); return setupTimeMs;

    }
}

