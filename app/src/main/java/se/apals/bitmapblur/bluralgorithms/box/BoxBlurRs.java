package se.apals.bitmapblur.bluralgorithms.box;

/**
 * Created by apals on 2017-03-13.
 */

import android.graphics.Bitmap;
import android.os.SystemClock;
import android.renderscript.Allocation;
import android.renderscript.RenderScript;

import java.util.concurrent.TimeUnit;

import se.apals.bitmapblur.BlurConstants;
import se.apals.bitmapblur.Blurrer;
import se.apals.bitmapblur.MemoryUtils;
import se.apals.bitmapblur.ScriptC_box_blur;

/**
 * Created by apals on 2017-03-07.
 */

public class BoxBlurRs implements Blurrer {

    final int radius = BlurConstants.RADIUS;
    private final RenderScript rs;

    public BoxBlurRs(RenderScript rs) {
        this.rs = rs;
    }

    @Override
    public String getName() {
        return "Box Blur Rs with radius " + radius;
    }

    @Override
    public long blur(Bitmap src, Bitmap dst) {
        long memoryUsedBefore = MemoryUtils.getUsedMemorySize();
        int w = src.getWidth();
        int h = src.getHeight();

        long start = SystemClock.elapsedRealtimeNanos();

        final Allocation input = Allocation.createFromBitmap(rs, src);
        final Allocation output = Allocation.createFromBitmap(rs, dst);


        final ScriptC_box_blur script = new ScriptC_box_blur(rs);


        script.set_width(w);
        script.set_height(h);

        //set input for blurring
        script.set_ScratchPixel1(input);
        script.set_ScratchPixel2(input);


        long end = SystemClock.elapsedRealtimeNanos();
        int setupTimeMs = (int) TimeUnit.NANOSECONDS.toMillis(end - start);

        // här blurrar vi
        script.forEach_horz(input);
        script.forEach_vert(output);


        // Maybe replace with Allocation.syncAll?
        output.copyTo(dst);
        rs.finish();
        long memoryUsedAfter = MemoryUtils.getUsedMemorySize(); return setupTimeMs;
    }
}

