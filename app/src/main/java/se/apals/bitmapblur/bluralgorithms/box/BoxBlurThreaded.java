package se.apals.bitmapblur.bluralgorithms.box;

import android.graphics.Bitmap;
import android.os.SystemClock;

import java.util.concurrent.TimeUnit;

import se.apals.bitmapblur.BlurConstants;
import se.apals.bitmapblur.Blurrer;
import se.apals.bitmapblur.MemoryUtils;

import static se.apals.bitmapblur.BlurConstants.BOX_5x5;

/**
 * Created by apals on 2017-03-06.
 */

public class BoxBlurThreaded implements Blurrer {

    private static final String TAG = "BoxBlurThreaded";
    final int radius = BlurConstants.RADIUS;
    final int pixels_on_row = 1 + radius * 2;
    final int availableProcessors;

    public BoxBlurThreaded() {
        availableProcessors = Runtime.getRuntime().availableProcessors();
    }

    @Override
    public String getName() {
        return "Box Blur Threaded Java with radius " + radius;
    }

    @Override
    public long blur(final Bitmap src, final Bitmap dst) {
        long memoryUsedBefore = MemoryUtils.getUsedMemorySize();
        long start = SystemClock.elapsedRealtimeNanos();
        final int height = src.getHeight();
        final int width = src.getWidth();

        final int[] srcpixels = new int[width * height];
        final int[] dstpixels = new int[width * height];
        src.getPixels(srcpixels, 0, width, 0, 0, width, height);

        long end = SystemClock.elapsedRealtimeNanos();
        int setupTimeMs = (int) TimeUnit.NANOSECONDS.toMillis(end - start);

        Thread[] threads = new Thread[availableProcessors];
        doHorizontalPass(height, width, srcpixels, dstpixels, threads);
        doVerticalPass(height, width, srcpixels, dstpixels, threads);

        dst.setPixels(dstpixels, 0, width, 0, 0, width, height);
        long memoryUsedAfter = MemoryUtils.getUsedMemorySize();
        return setupTimeMs;
    }

    private void doVerticalPass(final int height, final int width, final int[] srcpixels, final int[] dstpixels, Thread[] threads) {
        for (int i = 0; i < availableProcessors; i++) {
            final int finalI = i;
            Thread t = new Thread(new Runnable() {
                @Override
                public void run() {
                    int start = finalI * width / availableProcessors;
                    int end = (finalI + 1) * width / availableProcessors;
                    vertical(height, width, srcpixels, dstpixels, start, end);
                }
            }, "Thread " + i);
            threads[i] = t;
            t.start();
        }
        for (int i = 0; i < availableProcessors; i++) {
            try {
                threads[i].join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void doHorizontalPass(final int height, final int width, final int[] srcpixels, final int[] dstpixels, Thread[] threads) {
        for (int i = 0; i < availableProcessors; i++) {
            final int finalI = i;
            Thread t = new Thread(new Runnable() {
                @Override
                public void run() {
                    int start = finalI * height / availableProcessors;
                    int end = (finalI + 1) * height / availableProcessors;
                    horizontal(width, srcpixels, dstpixels, start, end);
                }
            }, "Thread " + i);
            threads[i] = t;
            t.start();
        }
        for (int i = 0; i < availableProcessors; i++) {
            try {
                threads[i].join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void vertical(int height, int width, int[] srcpixels, int[] dstpixels, int start, int end) {
        int offset;

        for (int y = 0; y < height; y++) {
            for (int x = start; x < end; x++) {
                int sumR = 0, sumG = 0, sumB = 0;
                offset = 0;
                for (int yoffset = -radius; yoffset <= radius; yoffset++) {
                    int sy = Math.max(0, Math.min(height - 1, y + yoffset));
                    int clr = dstpixels[width * sy + x];
                    sumR += ((clr >> 16) & 0xff) * BOX_5x5[offset];
                    sumG += ((clr >> 8) & 0xff) * BOX_5x5[offset];
                    sumB += (clr & 0xff) * BOX_5x5[offset];
                    offset++;
                }

                // get final Red
                if (sumR < 0) {
                    sumR = 0;
                } else if (sumR > 255) {
                    sumR = 255;
                }

                // get final Green
                if (sumG < 0) {
                    sumG = 0;
                } else if (sumG > 255) {
                    sumG = 255;
                }

                // get final Blue
                if (sumB < 0) {
                    sumB = 0;
                } else if (sumB > 255) {
                    sumB = 255;
                }

                int sy = Math.max(0, Math.min(height - 1, y));

                dstpixels[width * sy + x] = 0xff << 24 | ((sumR) << 16) | ((sumG) << 8) | (sumB);
            }
        }
    }

    private void horizontal(int width, int[] srcpixels, int[] dstpixels, int start, int end) {
        int offset;
        for (int y = start; y < end; y++) {
            int row = y * width;              ///< Specifies the current row
            for (int x = 0; x < width; x++) {
                int sumR = 0, sumG = 0, sumB = 0;
                offset = 0;
                for (int xoffset = -radius; xoffset <= radius; xoffset++) {

                    int sx = Math.max(0, Math.min(width - 1, x + xoffset));
                    int p = srcpixels[row + sx];
                    sumR += ((p >> 16) & 0xff) * BOX_5x5[offset];
                    sumG += ((p >> 8) & 0xff) * BOX_5x5[offset];
                    sumB += (p & 0xff) * BOX_5x5[offset];
                    offset++;

                }

                // get final Red
                if (sumR < 0) {
                    sumR = 0;
                } else if (sumR > 255) {
                    sumR = 255;
                }

                // get final Green
                if (sumG < 0) {
                    sumG = 0;
                } else if (sumG > 255) {
                    sumG = 255;
                }

                // get final Blue
                if (sumB < 0) {
                    sumB = 0;
                } else if (sumB > 255) {
                    sumB = 255;
                }

                int sx = Math.max(0, Math.min(width - 1, x));
                dstpixels[row + sx] = 0xff << 24 | ((sumR) << 16) | ((sumG) << 8) | (sumB);

            }
        }
    }
}
