package se.apals.bitmapblur.bluralgorithms.gammacorrection;

import android.graphics.Bitmap;
import android.os.SystemClock;

import java.util.concurrent.TimeUnit;

import se.apals.bitmapblur.BlurConstants;
import se.apals.bitmapblur.Blurrer;
import se.apals.bitmapblur.MemoryUtils;

import static se.apals.bitmapblur.BlurConstants.BOX_5x5;

/**
 * Created by apals on 2017-03-06.
 */

public class GammaCorrection implements Blurrer {

    private float correction = 1f/2.2f;

    @Override
    public String getName() {
        return "Gamma correction Java";
    }

    @Override
    public long blur(Bitmap src, Bitmap dst) {

        int height = src.getHeight();
        int width = src.getWidth();

        long start = SystemClock.elapsedRealtimeNanos();

        int[] srcpixels = new int[width * height];
        int[] dstpixels = new int[width * height];
        src.getPixels(srcpixels, 0, width, 0, 0, width, height);


        long end = SystemClock.elapsedRealtimeNanos();
        int setupTimeMs = (int) TimeUnit.NANOSECONDS.toMillis(end - start);

        for (int i = 0; i < srcpixels.length; i++) {
            int p = srcpixels[i];
            float sumR = ((p >> 16) & 0xff);
            float sumG = ((p >> 8) & 0xff);
            float sumB = (p & 0xff);
//            int newRed = (int) Math.round(255 - Math.pow(sumR / 255.0, correction));
//            int newGreen = (int) Math.round(255 - Math.pow(sumG / 255.0, correction));
//            int newBlue = (int) Math.round(255 - Math.pow(sumB / 255.0, correction));

            int newRed = (int) (255f * Math.pow((sumR / 255f), correction));
            int newGreen = (int) (255f * Math.pow((sumG  / 255f), correction));
            int newBlue  = (int) (255f * Math.pow((sumB  / 255f), correction));
            dstpixels[i] = 0xff << 24 | ((newRed) << 16) | ((newGreen) << 8) | (newBlue);
        }


        dst.setPixels(dstpixels, 0, width, 0, 0, width, height);
        return setupTimeMs;
    }
}
