package se.apals.bitmapblur.bluralgorithms.gaussian;

import android.graphics.Bitmap;
import android.os.SystemClock;

import java.util.concurrent.TimeUnit;

import se.apals.bitmapblur.BlurConstants;
import se.apals.bitmapblur.Blurrer;
import se.apals.bitmapblur.MemoryUtils;

import static se.apals.bitmapblur.BlurConstants.GAUSSIAN_5x5;

/**
 * Created by apals on 2017-03-06.
 */

public class GaussianBlur implements Blurrer {

    final int radius = BlurConstants.RADIUS;
    final int pixels_on_row = 1 + radius * 2;

    @Override
    public String getName() {
        return "Gaussian Blur Java with radius " + radius;
    }

    @Override
    public long blur(Bitmap src, Bitmap dst) {
        long memoryUsedBefore = MemoryUtils.getUsedMemorySize();
        long start = SystemClock.elapsedRealtimeNanos();

        int height = src.getHeight();
        int width = src.getWidth();

        int[] srcpixels = new int[width * height];
        int[] dstpixels = new int[width * height];
        src.getPixels(srcpixels, 0, width, 0, 0, width, height);

        long end = SystemClock.elapsedRealtimeNanos();
        int setupTimeMs = (int) TimeUnit.NANOSECONDS.toMillis(end - start);

        int offset;
        int i = 0;
        for (int y = 0; y < height; y++) {
            int row = y * width;              ///< Specifies the current row
            for (int x = 0; x < width; x++) {
                int sumR = 0, sumG = 0, sumB = 0;
                offset = 0;

                for (int xoffset = -radius; xoffset <= radius; xoffset++) {

                    int sx = Math.max(0, Math.min(width - 1, x + xoffset));
                    int p = srcpixels[row + sx];
                    sumR += ((p >> 16) & 0xff) * GAUSSIAN_5x5[offset];
                    sumG += ((p >> 8) & 0xff) * GAUSSIAN_5x5[offset];
                    sumB += (p & 0xff) * GAUSSIAN_5x5[offset];
                    offset++;

                }

                // get final Red
                if (sumR < 0) {
                    sumR = 0;
                } else if (sumR > 255) {
                    sumR = 255;
                }

                // get final Green
                if (sumG < 0) {
                    sumG = 0;
                } else if (sumG > 255) {
                    sumG = 255;
                }

                // get final Blue
                if (sumB < 0) {
                    sumB = 0;
                } else if (sumB > 255) {
                    sumB = 255;
                }

                dstpixels[i++] = 0xff << 24 | ((sumR) << 16) | ((sumG) << 8) | (sumB);
            }
        }

        i = 0;
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                int sumR = 0, sumG = 0, sumB = 0;
                offset = 0;
                for (int yoffset = -radius; yoffset <= radius; yoffset++) {
                    int sy = Math.max(0, Math.min(height - 1, y + yoffset));
                    int clr = dstpixels[width * sy + x];
                    sumR += ((clr >> 16) & 0xff) * GAUSSIAN_5x5[offset];
                    sumG += ((clr >> 8) & 0xff) * GAUSSIAN_5x5[offset];
                    sumB += (clr & 0xff) * GAUSSIAN_5x5[offset];
                    offset++;
                }

                // get final Red
                if (sumR < 0) {
                    sumR = 0;
                } else if (sumR > 255) {
                    sumR = 255;
                }

                // get final Green
                if (sumG < 0) {
                    sumG = 0;
                } else if (sumG > 255) {
                    sumG = 255;
                }

                // get final Blue
                if (sumB < 0) {
                    sumB = 0;
                } else if (sumB > 255) {
                    sumB = 255;
                }

                dstpixels[i++] = 0xff << 24 | ((sumR) << 16) | ((sumG) << 8) | (sumB);
            }
        }

        dst.setPixels(dstpixels, 0, width, 0, 0, width, height);
        long memoryUsedAfter = MemoryUtils.getUsedMemorySize(); return setupTimeMs;
    }
}
