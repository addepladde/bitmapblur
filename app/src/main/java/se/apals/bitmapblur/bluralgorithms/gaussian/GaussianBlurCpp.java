package se.apals.bitmapblur.bluralgorithms.gaussian;

import android.graphics.Bitmap;
import android.os.SystemClock;

import java.util.concurrent.TimeUnit;

import se.apals.bitmapblur.BlurConstants;
import se.apals.bitmapblur.Blurrer;
import se.apals.bitmapblur.MemoryUtils;

/**
 * Created by apals on 2017-03-06.
 */

public class GaussianBlurCpp implements Blurrer {

    static {
        System.loadLibrary("native-lib");
    }

    final int radius = BlurConstants.RADIUS;

    /**
     * A native method that is implemented by the 'native-lib' native library,
     * which is packaged with this application.
     */
    public native void gaussianBlur(int[] src, int[] dst, int width, int height);

    @Override
    public String getName() {
        return "Gaussian Blur Cpp with radius " + radius;
    }

    @Override
    public long blur(Bitmap src, Bitmap dst) {
        long memoryUsedBefore = MemoryUtils.getUsedMemorySize();
        long start = SystemClock.elapsedRealtimeNanos();

        int height = src.getHeight();
        int width = src.getWidth();

        int[] pixels = new int[height * width];
        int[] dstpixels = new int[height * width];
        src.getPixels(pixels, 0, width, 0, 0, width, height);
        long end = SystemClock.elapsedRealtimeNanos();
        int setupTimeMs = (int) TimeUnit.NANOSECONDS.toMillis(end - start);
        gaussianBlur(pixels, dstpixels, width, height);
        dst.setPixels(dstpixels, 0, width, 0, 0, width, height);
        long memoryUsedAfter = MemoryUtils.getUsedMemorySize(); return setupTimeMs;
    }
}
