package se.apals.bitmapblur.bluralgorithms.gaussian;

import android.graphics.Bitmap;
import android.os.SystemClock;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;

import java.util.concurrent.TimeUnit;

import se.apals.bitmapblur.BlurConstants;
import se.apals.bitmapblur.Blurrer;
import se.apals.bitmapblur.MemoryUtils;

/**
 * Created by apals on 2017-03-07.
 */

public class GaussianBlurIntrinsicRs implements Blurrer {

    final int radius = BlurConstants.RADIUS;
    private final RenderScript rs;

    public GaussianBlurIntrinsicRs(RenderScript rs) {
        this.rs = rs;
    }

    @Override
    public String getName() {
        return "Gaussian Blur Intrinsic Rs with radius " + radius;
    }

    @Override
    public long blur(Bitmap src, Bitmap dst) {
        long memoryUsedBefore = MemoryUtils.getUsedMemorySize();
        long start = SystemClock.elapsedRealtimeNanos();
        final Allocation input = Allocation.createFromBitmap(rs, src);
        final Allocation output = Allocation.createTyped(rs, input.getType());
        final ScriptIntrinsicBlur script = ScriptIntrinsicBlur.create(rs, Element.U8_4(rs));
        script.setRadius(radius);
        script.setInput(input);
        long end = SystemClock.elapsedRealtimeNanos();
        int setupTimeMs = (int) TimeUnit.NANOSECONDS.toMillis(end - start);
        script.forEach(output);
        output.copyTo(dst);

        long memoryUsedAfter = MemoryUtils.getUsedMemorySize();
        return setupTimeMs;
    }
}
