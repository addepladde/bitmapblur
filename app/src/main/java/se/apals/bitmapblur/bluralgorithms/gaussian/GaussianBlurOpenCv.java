package se.apals.bitmapblur.bluralgorithms.gaussian;

import android.graphics.Bitmap;
import android.os.SystemClock;

import org.opencv.android.Utils;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;

import java.util.concurrent.TimeUnit;

import se.apals.bitmapblur.BlurConstants;
import se.apals.bitmapblur.Blurrer;
import se.apals.bitmapblur.MemoryUtils;

import static se.apals.bitmapblur.BlurConstants.GAUSSIAN_5x5;

/**
 * Created by apals on 2017-03-06.
 */

public class GaussianBlurOpenCv implements Blurrer {

    final int radius = BlurConstants.RADIUS;
    final Mat kernel = new Mat(GAUSSIAN_5x5.length, 1, CvType.CV_32FC1);

    static {
        System.loadLibrary("opencv_java3");
    }

    public GaussianBlurOpenCv() {
        kernel.put(0, 0, GAUSSIAN_5x5);
    }

    @Override
    public String getName() {
        return "Gaussian Blur OpenCv Cpp with radius " + radius;
    }

    @Override
    public long blur(Bitmap src, Bitmap dst) {
        long memoryUsedBefore = MemoryUtils.getUsedMemorySize();
        long start = SystemClock.elapsedRealtimeNanos();

        Mat srcMat = new Mat();
        Mat dstMat = new Mat();

        Utils.bitmapToMat(src, srcMat);
        long end = SystemClock.elapsedRealtimeNanos();
        int setupTimeMs = (int) TimeUnit.NANOSECONDS.toMillis(end - start);

        // -1 indicates that we want the same depth as the source image
        Imgproc.filter2D(srcMat, dstMat, -1, kernel);
//        Log.d("GaussianBlurOpenCv", kernel.dump());


        Utils.matToBitmap(dstMat, dst);
        long memoryUsedAfter = MemoryUtils.getUsedMemorySize(); return setupTimeMs;
    }
}

