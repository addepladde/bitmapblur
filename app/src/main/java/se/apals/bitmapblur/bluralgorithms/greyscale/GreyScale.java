package se.apals.bitmapblur.bluralgorithms.greyscale;

import android.graphics.Bitmap;
import android.os.SystemClock;
import android.renderscript.Allocation;
import android.renderscript.RenderScript;
import android.renderscript.ScriptC;

import java.util.concurrent.TimeUnit;

import se.apals.bitmapblur.Blurrer;
import se.apals.bitmapblur.ScriptC_greyscale;

/**
 * Created by apals on 2017-05-08.
 */

public class GreyScale implements Blurrer {

    private final RenderScript rs;

    public GreyScale(RenderScript rs) {
        this.rs = rs;
    }

    @Override
    public String getName() {
        return "GRAYSCALE RS";
    }

    @Override
    public long blur(Bitmap src, Bitmap dst) {
        long start = SystemClock.elapsedRealtimeNanos();
        Allocation mInAllocation = Allocation.createFromBitmap(rs, src,
                Allocation.MipmapControl.MIPMAP_NONE,
                Allocation.USAGE_SCRIPT);
        Allocation mOutAllocation = Allocation.createTyped(rs, mInAllocation.getType());
        ScriptC_greyscale mScript = new ScriptC_greyscale(rs);

        long end = SystemClock.elapsedRealtimeNanos();
        int setupTimeMs = (int) TimeUnit.NANOSECONDS.toMillis(end - start);

        mScript.forEach_root(mInAllocation, mOutAllocation);
        mOutAllocation.copyTo(dst);
        return setupTimeMs;
    }
}
