package se.apals.bitmapblur.bluralgorithms.greyscale;

import android.graphics.Bitmap;
import android.os.SystemClock;
import android.renderscript.Allocation;
import android.renderscript.RenderScript;
import android.renderscript.ScriptC;

import java.util.concurrent.TimeUnit;

import se.apals.bitmapblur.Blurrer;
import se.apals.bitmapblur.ScriptC_greyscale;
import se.apals.bitmapblur.ScriptC_greyscalerelaxed;

/**
 * Created by apals on 2017-05-08.
 */

public class GreyScaleRelaxed implements Blurrer {

    private final RenderScript rs;

    public GreyScaleRelaxed(RenderScript rs) {
        this.rs = rs;
    }

    @Override
    public String getName() {
        return "GRAYSCALE Relaxed RS";
    }

    @Override
    public long blur(Bitmap src, Bitmap dst) {
        long start = SystemClock.elapsedRealtimeNanos();
        Allocation mInAllocation = Allocation.createFromBitmap(rs, src,
                Allocation.MipmapControl.MIPMAP_NONE,
                Allocation.USAGE_SCRIPT);
        Allocation mOutAllocation = Allocation.createTyped(rs, mInAllocation.getType());
        ScriptC_greyscalerelaxed mScript = new ScriptC_greyscalerelaxed(rs);

        long end = SystemClock.elapsedRealtimeNanos();
        int setupTimeMs = (int) TimeUnit.NANOSECONDS.toMillis(end - start);

        mScript.forEach_root(mInAllocation, mOutAllocation);
        mOutAllocation.copyTo(dst);
        return setupTimeMs;
    }
}
