package se.apals.bitmapblur.bluralgorithms.greyscale;

/**
 * Created by apals on 2017-05-09.
 */

import android.graphics.Bitmap;
import android.os.SystemClock;

import java.util.concurrent.TimeUnit;

import se.apals.bitmapblur.BlurConstants;
import se.apals.bitmapblur.Blurrer;
import se.apals.bitmapblur.MemoryUtils;


import android.graphics.Bitmap;
import android.os.SystemClock;

import java.util.concurrent.TimeUnit;

import se.apals.bitmapblur.BlurConstants;
import se.apals.bitmapblur.Blurrer;
import se.apals.bitmapblur.MemoryUtils;

/**
 * Created by apals on 2017-03-06.
 */

public class GreyScaleThreadedCpp implements Blurrer {

    static {
        System.loadLibrary("native-lib");
    }

    final int radius = BlurConstants.RADIUS;

    final int availableProcessors;

    public GreyScaleThreadedCpp() {
        availableProcessors = Runtime.getRuntime().availableProcessors();
    }

    /**
     * A native method that is implemented by the 'native-lib' native library,
     * which is packaged with this application.
     */
    public native void greyscale(int[] src, int[] dst, int width, int height, int availableProcessors);

    @Override
    public String getName() {
        return "Gray scale Threaded Cpp";
    }

    @Override
    public long blur(Bitmap src, Bitmap dst) {
        long start = SystemClock.elapsedRealtimeNanos();

        int height = src.getHeight();
        int width = src.getWidth();

        int[] pixels = new int[height * width];
        int[] dstpixels = new int[height * width];
        src.getPixels(pixels, 0, width, 0, 0, width, height);

        long end = SystemClock.elapsedRealtimeNanos();
        int setupTimeMs = (int) TimeUnit.NANOSECONDS.toMillis(end - start);


        greyscale(pixels, dstpixels, width, height, availableProcessors);

        dst.setPixels(dstpixels, 0, width, 0, 0, width, height);
        return setupTimeMs;
    }
}

