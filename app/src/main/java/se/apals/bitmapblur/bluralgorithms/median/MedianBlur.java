package se.apals.bitmapblur.bluralgorithms.median;

import android.graphics.Bitmap;
import android.os.SystemClock;

import java.util.concurrent.TimeUnit;

import se.apals.bitmapblur.BlurConstants;
import se.apals.bitmapblur.Blurrer;
import se.apals.bitmapblur.MemoryUtils;


/**
 * Created by apals on 2017-03-06.
 */

public class MedianBlur implements Blurrer {

    final int radius = BlurConstants.RADIUS;

    @Override
    public String getName() {
        return "Median Blur Java with radius " + radius;
    }

    @Override
    public long blur(Bitmap src, Bitmap dst) {
        long memoryUsedBefore = MemoryUtils.getUsedMemorySize();
        long start= SystemClock.elapsedRealtimeNanos();

        int height = src.getHeight();
        int width = src.getWidth();

        int[] srcpixels = new int[width * height];
        int[] dstpixels = new int[width * height];
        src.getPixels(srcpixels, 0, width, 0, 0, width, height);
        long end = SystemClock.elapsedRealtimeNanos();
        int setupTimeMs = (int) TimeUnit.NANOSECONDS.toMillis(end - start);

        int[] clr = new int[radius * radius];

        /** Median Filter operation */
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                int count = 0;
                for (int r = y - (radius / 2); r <= y + (radius / 2); r++) {
                    for (int c = x - (radius / 2); c <= x + (radius / 2); c++) {
                        if (r >= 0 && r < height && c >= 0 && c < width) {
                            int px = srcpixels[r * width + c];
                            clr[count] = px;
                            count++;
                        }
                    }
                }

                java.util.Arrays.sort(clr, 0, count);
//                int median = Util.median(clr);

                /** save median value in outputPixels array */
                int index = (count % 2 == 0) ? count / 2 - 1 : count / 2;
//                dstpixels[x + y * width] = 0xff << 24 | ((red[index]) << 16) | ((green[index]) << 8) | (blue[index]);
                dstpixels[x + y * width] = clr[index];
//                dstpixels[x + y * width] = median;


            }
        }
        dst.setPixels(dstpixels, 0, width, 0, 0, width, height);
        long memoryUsedAfter = MemoryUtils.getUsedMemorySize();
        return setupTimeMs;
    }
}
