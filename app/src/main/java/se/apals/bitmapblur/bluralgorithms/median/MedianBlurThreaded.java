package se.apals.bitmapblur.bluralgorithms.median;

import android.graphics.Bitmap;
import android.os.SystemClock;

import java.util.concurrent.TimeUnit;

import se.apals.bitmapblur.Blurrer;
import se.apals.bitmapblur.MemoryUtils;


/**
 * Created by apals on 2017-03-06.
 */

public class MedianBlurThreaded implements Blurrer {

    final int radius = 5;
    private final int availableProcessors;

    public MedianBlurThreaded() {
        availableProcessors = Runtime.getRuntime().availableProcessors();
    }

    @Override
    public String getName() {
        return "Median Blur Threaded Java with radius " + radius;
    }

    @Override
    public long blur(final Bitmap src, final Bitmap dst) {
        long memoryUsedBefore = MemoryUtils.getUsedMemorySize();
        long start = SystemClock.elapsedRealtimeNanos();

        final int height = src.getHeight();
        final int width = src.getWidth();

        final int[] srcpixels = new int[width * height];
        final int[] dstpixels = new int[width * height];
        src.getPixels(srcpixels, 0, width, 0, 0, width, height);

        Thread[] threads = new Thread[availableProcessors];
        long end = SystemClock.elapsedRealtimeNanos();
        int setupTimeMs = (int) TimeUnit.NANOSECONDS.toMillis(end - start);
        for (int i = 0; i < availableProcessors; i++) {
            final int finalI = i;
            Thread t = new Thread(new Runnable() {
                @Override
                public void run() {
                    int start = finalI * height / availableProcessors;
                    int end = (finalI + 1) * height / availableProcessors;
                    medianBlur(srcpixels, dstpixels, start, end, height, width);
                }
            }, "Thread " + i);
            threads[i] = t;
            t.start();
        }

        for (int i = 0; i < availableProcessors; i++) {
            try {
                threads[i].join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        dst.setPixels(dstpixels, 0, width, 0, 0, width, height);
        long memoryUsedAfter = MemoryUtils.getUsedMemorySize();
        return setupTimeMs;
    }

    private void medianBlur(int[] src, int[] dst, int start, int end, int height, int width) {
        int[] clr = new int[radius * radius];
        /** Median Filter operation */
        for (int y = start; y < end; y++) {
            for (int x = 0; x < width; x++) {
                int count = 0;
                for (int r = y - (radius / 2); r <= y + (radius / 2); r++) {
                    for (int c = x - (radius / 2); c <= x + (radius / 2); c++) {
                        if (r >= 0 && r < height && c >= 0 && c < width) {
                            int px = src[r * width + c];
                            clr[count] = px;
                            count++;
                        }
                    }
                }

                /** sort red, green, blue array */
                java.util.Arrays.sort(clr, 0, count);

                /** save median value in outputPixels array */
                int index = (count % 2 == 0) ? count / 2 - 1 : count / 2;
//                dst[x + y * width] = 0xff << 24 | ((red[index]) << 16) | ((green[index]) << 8) | (blue[index]);
                dst[x + y * width] = clr[index];
            }
        }
    }
}
