package se.apals.bitmapblur.bluralgorithms.thresholding;

import android.graphics.Bitmap;
import android.os.SystemClock;

import org.opencv.android.Utils;
import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;

import java.util.concurrent.TimeUnit;

import se.apals.bitmapblur.BlurConstants;
import se.apals.bitmapblur.Blurrer;
import se.apals.bitmapblur.MemoryUtils;

/**
 * Created by apals on 2017-03-06.
 */

public class ThresholdingOpenCv implements Blurrer {

    final int radius = BlurConstants.RADIUS;

    static {
        System.loadLibrary("opencv_java3");
    }

    @Override
    public String getName() {
        return "OpenCV Thresholding " + radius;
    }

    @Override
    public long blur(Bitmap src, Bitmap dst) {
        long memoryUsedBefore = MemoryUtils.getUsedMemorySize();
        long start = SystemClock.elapsedRealtimeNanos();

        Mat srcMat = new Mat();
//        Mat tmpMat = new Mat();
        Mat dstMat = new Mat();

        Utils.bitmapToMat(src, srcMat);
        long end = SystemClock.elapsedRealtimeNanos();
        int setupTimeMs = (int) TimeUnit.NANOSECONDS.toMillis(end - start);

        Imgproc.cvtColor(srcMat, dstMat, Imgproc.COLOR_RGB2GRAY);
        Imgproc.threshold(dstMat, dstMat, 127.5f, 255, Imgproc.THRESH_BINARY);

        Utils.matToBitmap(dstMat, dst);
        long memoryUsedAfter = MemoryUtils.getUsedMemorySize();
        return setupTimeMs;

    }
}

