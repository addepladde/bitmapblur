package se.apals.bitmapblur.bluralgorithms.thresholding;

import android.graphics.Bitmap;
import android.os.SystemClock;
import android.renderscript.Allocation;
import android.renderscript.RenderScript;
import android.renderscript.ScriptC;

import java.util.concurrent.TimeUnit;

import se.apals.bitmapblur.Blurrer;
import se.apals.bitmapblur.ScriptC_greyscale;
import se.apals.bitmapblur.ScriptC_thresholding;
import se.apals.bitmapblur.ScriptC_thresholdingrelaxed;

/**
 * Created by apals on 2017-05-08.
 */

public class ThresholdingRsRelaxed implements Blurrer {

    private final RenderScript rs;

    public ThresholdingRsRelaxed(RenderScript rs) {
        this.rs = rs;
    }

    @Override
    public String getName() {
        return "Thresholding Relaxed RS";
    }

    @Override
    public long blur(Bitmap src, Bitmap dst) {
        long start = SystemClock.elapsedRealtimeNanos();
        Allocation mInAllocation = Allocation.createFromBitmap(rs, src,
                Allocation.MipmapControl.MIPMAP_NONE,
                Allocation.USAGE_SCRIPT);
        Allocation mOutAllocation = Allocation.createTyped(rs, mInAllocation.getType());
        ScriptC_thresholdingrelaxed mScript = new ScriptC_thresholdingrelaxed(rs);
        mScript.set_threshold(0.5f);

        long end = SystemClock.elapsedRealtimeNanos();
        int setupTimeMs = (int) TimeUnit.NANOSECONDS.toMillis(end - start);

        mScript.forEach_root(mInAllocation, mOutAllocation);
        mOutAllocation.copyTo(dst);
        return setupTimeMs;
    }
}
