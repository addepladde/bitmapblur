package se.apals.bitmapblur.bluralgorithms.thresholding;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.SystemClock;

import java.util.concurrent.TimeUnit;

import se.apals.bitmapblur.Blurrer;

/**
 * Created by apals on 2017-05-08.
 */

public class ThresholdingThreadedJava implements Blurrer {


    final int availableProcessors;

    public ThresholdingThreadedJava() {
        availableProcessors = Runtime.getRuntime().availableProcessors();
    }


    @Override
    public String getName() {
        return "Thresholding Java";
    }

    @Override
    public long blur(Bitmap src, Bitmap dst) {
        long start = SystemClock.elapsedRealtimeNanos();
        final int height = src.getHeight();
        final int width = src.getWidth();

        final int[] srcpixels = new int[width * height];
        final int[] dstpixels = new int[width * height];
        src.getPixels(srcpixels, 0, width, 0, 0, width, height);
        long end = SystemClock.elapsedRealtimeNanos();
        int setupTimeMs = (int) TimeUnit.NANOSECONDS.toMillis(end - start);

        final int step = srcpixels.length / availableProcessors;

        Thread[] threads = new Thread[availableProcessors];
        for (int i = 0; i < availableProcessors; i++) {
            final int finalI = i;
            Thread t = new Thread(new Runnable() {
                @Override
                public void run() {
                    for (int idx = finalI * step; idx < (finalI + 1) * step; idx++) {
                        int pixel = srcpixels[idx];
                        int r = ((pixel >> 16) & 0xff);
                        int g = ((pixel >> 8) & 0xff);
                        int b = (pixel & 0xff);
                        r = (int) (0.299 * r + 0.587 * g + 0.114 * b);
                        if(r < 127.5f) {
                            r = 0xff000000;
                        } else {
                            r = 0xffffffff;
                        }
                        dstpixels[idx] = r;
                    }

                }
            });
            threads[i] = t;
            t.start();
        }

        for (Thread thread : threads) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }


        dst.setPixels(dstpixels, 0, width, 0, 0, width, height);
        return setupTimeMs;
    }
}
