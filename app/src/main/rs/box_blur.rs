#pragma version(1)
#pragma rs java_package_name(se.apals.bitmapblur)
#pragma rs_fp_relaxed

int height;
int width;
static int radius = 5;

rs_allocation ScratchPixel1;
rs_allocation ScratchPixel2;

const int MAX_RADIUS = 25;
// Store our coefficients here
// static float gaussian[MAX_RADIUS * 2 + 1];
static float gaussian[]  = {
                                    (float) 1/11,
                                    (float) 1/11,
                                    (float) 1/11,
                                    (float) 1/11,
                                    (float) 1/11,
                                    (float) 1/11,
                                    (float) 1/11,
                                    (float) 1/11,
                                    (float) 1/11,
                                    (float) 1/11,
                                    (float) 1/11,
                            };

uchar4 __attribute__((kernel)) vert(uint32_t x, uint32_t y) {
    int gi = 0;
    uchar red = 0;
    uchar g = 0;
    uchar b = 0;
    for (int r = -radius; r <= radius; r ++) {
        int validH = clamp((int)y + r, (int)0, (int)(height - 1));
        uchar4 j = rsGetElementAt_uchar4(ScratchPixel2, x, validH).rgba;
        red += j[0] * gaussian[gi];
        g += j[1] * gaussian[gi];
        b += j[2] * gaussian[gi];
        gi++;
    }
    //rgba
    return (uchar4) {red, g, b, 255};
}

uchar4 __attribute__((kernel)) horz(uint32_t x, uint32_t y) {
    int gi = 0;
    uchar red = 0;
    uchar g = 0;
    uchar b = 0;

    for (int r = -radius; r <= radius; r ++) {
        // Stepping left and right away from the pixel
        int validX = clamp((int)x + r, (int)0, (int)(width - 1));
        uchar4 i = rsGetElementAt_uchar4(ScratchPixel1, validX, y).rgba;
        red += i[0] * gaussian[gi];
        g += i[1] * gaussian[gi];
        b += i[2] * gaussian[gi];

        gi++;
    }

    //rgba
    return (uchar4) {red, g, b, 255};
}