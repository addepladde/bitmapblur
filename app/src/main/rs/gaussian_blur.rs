#pragma version(1)
#pragma rs java_package_name(se.apals.bitmapblur)
#pragma rs_fp_relaxed

int height;
int width;
static int radius = 5;

rs_allocation ScratchPixel1;
rs_allocation ScratchPixel2;

const int MAX_RADIUS = 25;
// Store our coefficients here
// static float gaussian[MAX_RADIUS * 2 + 1];
static float gaussian[]  = {
                                    0.000003f,
                                    0.000229f,
                                    0.005977f,
                                    0.060598f,
                                    0.24173f,
                                    0.382925f,
                                    0.24173f,
                                    0.060598f,
                                    0.005977f,
                                    0.000229f,
                                    0.000003f
                            };
float4 __attribute__((kernel)) convert1_uToF(uchar v) {
    float4 r = rsUnpackColor8888(v);
    return r.r;
}
float4 __attribute__((kernel)) convert4_uToF(uchar4 v) {
    return rsUnpackColor8888(v);
}
uchar __attribute__((kernel)) convert1_fToU(float4 v) {
    uchar4 r = rsPackColorTo8888(v);
    return r.r;
}
uchar4 __attribute__((kernel)) convert4_fToU(float4 v) {
    return rsPackColorTo8888(v);
}
void setRadius(int rad) {
    // This function is a duplicate of:
    // RsdCpuScriptIntrinsicBlur::ComputeGaussianWeights()
    // Which is the reference C implementation
    radius = rad;
    const float e = M_E;
    const float pi = M_PI;
    float sigma = 0.4f * (float)radius + 0.6f;
    float coeff1 = 1.0f / (sqrt( 2.0f * pi ) * sigma);
    float coeff2 = - 1.0f / (2.0f * sigma * sigma);
    float normalizeFactor = 0.0f;
    float floatR = 0.0f;
    for (int r = -radius; r <= radius; r ++) {
        floatR = (float)r;
        gaussian[r + radius] = coeff1 * pow(e, floatR * floatR * coeff2);
        normalizeFactor += gaussian[r + radius];
    }
    normalizeFactor = 1.0f / normalizeFactor;
    for (int r = -radius; r <= radius; r ++) {
        floatR = (float)r;
        gaussian[r + radius] *= normalizeFactor;
    }
}

uchar4 __attribute__((kernel)) vert(uint32_t x, uint32_t y) {
    int gi = 0;
    uchar red = 0;
    uchar g = 0;
    uchar b = 0;
    for (int r = -radius; r <= radius; r ++) {
        int validH = clamp((int)y + r, (int)0, (int)(height - 1));
        uchar4 j = rsGetElementAt_uchar4(ScratchPixel2, x, validH).rgba;
        red += j[0] * gaussian[gi];
        g += j[1] * gaussian[gi];
        b += j[2] * gaussian[gi];
        gi++;
    }
    //rgba
    return (uchar4) {red, g, b, 255};
}

uchar4 __attribute__((kernel)) horz(uint32_t x, uint32_t y) {
    int gi = 0;
    uchar red = 0;
    uchar g = 0;
    uchar b = 0;

    for (int r = -radius; r <= radius; r ++) {
        // Stepping left and right away from the pixel
        int validX = clamp((int)x + r, (int)0, (int)(width - 1));
        uchar4 i = rsGetElementAt_uchar4(ScratchPixel1, validX, y).rgba;
        red += i[0] * gaussian[gi];
        g += i[1] * gaussian[gi];
        b += i[2] * gaussian[gi];

        gi++;
    }

    //rgba
    return (uchar4) {red, g, b, 255};
}