#pragma version(1)
#pragma rs java_package_name(se.apals.bitmapblur)
#pragma rs_fp_relaxed


const static float3 gMonoMult = {0.299f, 0.587f, 0.114f};
float threshold;


uchar4 __attribute__((kernel)) root(uint32_t v_in) {
    float4 f4 = rsUnpackColor8888(v_in);
    float val = f4.r + f4.g + 0.114f * f4.b;
  	val = val > threshold ? 1 : 0;
  	f4.r = f4.g = f4.b = val;
  	float3 output = {f4.r, f4.g, f4.b};

    return rsPackColorTo8888(output);
}
uchar __attribute__((kernel)) toU8(uchar4 v_in) {
    float4 f4 = convert_float4(v_in);
    return (uchar)dot(f4.rgb, gMonoMult);
}
uchar4 __attribute__((kernel)) toU8_4(uchar v_in) {
    return (uchar4)v_in;
}