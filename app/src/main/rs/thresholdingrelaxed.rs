#pragma version(1)
#pragma rs java_package_name(se.apals.bitmapblur)
#pragma rs_fp_relaxed


const static float3 gMonoMult = {0.299f, 0.587f, 0.114f};
float threshold;


uchar4 RS_KERNEL root(uchar4 v_in) {
    float4 f4 = rsUnpackColor8888(v_in);
    float val = 0.299 * f4.r + 0.587 * f4.g + 0.114 * f4.b;
  	val = val > threshold ? 1 : 0;
  	f4.r = f4.g = f4.b = val;
  	float3 output = {f4.r, f4.g, f4.b};

    return rsPackColorTo8888(output);
}
uchar RS_KERNEL toU8(uchar4 v_in) {
    float4 f4 = convert_float4(v_in);
    return (uchar)dot(f4.rgb, gMonoMult);
}
uchar4 RS_KERNEL toU8_4(uchar v_in) {
    return (uchar4)v_in;
}