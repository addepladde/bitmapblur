 ------------------------
 BOX Blur Java with radius 5
 (2073600, 4907)
 avg: 4907, min: 4693, max: 5164 ms, memoryUsed: 35
 times: [4896, 5129, 4791, 4909, 5037, 4848, 4879, 4988, 5137, 4772, 5164, 4980, 4868, 5127, 4850, 4868, 4835, 4849, 4839, 5003, 4931, 4768, 4948, 4693, 4993, 4794, 4698, 4838, 4864, 4912, ]
 ------------------------
 Box Blur Threaded Java with radius 5
 (2073600, 1084)
 avg: 1084, min: 994, max: 1259 ms, memoryUsed: 34
 times: [1041, 1000, 1026, 1109, 1035, 1049, 994, 1036, 1022, 1131, 1081, 1063, 1030, 1085, 1061, 1041, 1063, 1065, 1101, 1120, 1080, 1108, 1225, 1084, 1101, 1116, 1127, 1171, 1099, 1259, ]
: ------------------------
: Box Blur Cpp with radius 5
: (2073600, 939)
: avg: 939, min: 896, max: 979 ms, memoryUsed: 33
: times: [949, 935, 932, 925, 945, 973, 979, 927, 955, 977, 931, 915, 934, 900, 923, 953, 908, 973, 933, 936, 971, 927, 896, 976, 948, 923, 903, 979, 916, 916, ]
 ------------------------
 Box Blur Threaded Cpp with radius 5
 (2073600, 311)
 avg: 311, min: 261, max: 402 ms, memoryUsed: 32
 times: [261, 309, 280, 289, 304, 290, 307, 330, 307, 402, 306, 323, 290, 305, 284, 314, 321, 393, 285, 323, 263, 326, 268, 332, 387, 299, 311, 350, 314, 269, ]
 ------------------------
 Box Blur OpenCv Cpp with radius 5
 (2073600, 236)
 avg: 236, min: 171, max: 320 ms, memoryUsed: 13
 times: [262, 280, 217, 247, 243, 211, 171, 219, 202, 308, 320, 206, 200, 186, 210, 228, 202, 205, 245, 212, 286, 229, 317, 205, 246, 268, 229, 223, 236, 266, ]
------------------------
Box Blur Rs with radius 5
(2073600, 324)
avg: 324, min: 289, max: 372 ms, memoryUsed: 55
times: [293, 291, 289, 327, 326, 311, 311, 319, 307, 359, 321, 312, 325, 342, 330, 328, 339, 304, 344, 303, 345, 312, 326, 344, 313, 372, 323, 342, 339, 325, ]
------------------------
Gaussian Blur Java with radius 5
(2073600, 4959)
avg: 4959, min: 4639, max: 5198 ms, memoryUsed: 32
times: [5017, 5014, 4902, 4910, 4817, 5050, 4928, 4926, 4895, 5003, 4923, 5198, 4838, 4910, 4911, 5113, 4783, 5043, 5088, 5053, 4943, 5020, 4900, 5049, 4931, 5185, 4639, 4861, 4981, 4940, ]
 ------------------------
 Gaussian Blur Threaded Java with radius 5
 (2073600, 1115)
 avg: 1115, min: 1049, max: 1187 ms, memoryUsed: 34
 times: [1052, 1068, 1049, 1095, 1089, 1114, 1064, 1136, 1055, 1087, 1095, 1104, 1096, 1138, 1108, 1165, 1109, 1117, 1183, 1142, 1108, 1112, 1137, 1187, 1111, 1183, 1102, 1121, 1184, 1125, ]
 ------------------------
 Gaussian Blur Cpp with radius 5
 (2073600, 939)
 avg: 939, min: 891, max: 1064 ms, memoryUsed: 33
 times: [982, 942, 950, 981, 896, 921, 932, 953, 953, 915, 969, 951, 936, 1064, 903, 966, 891, 919, 923, 893, 913, 927, 915, 922, 950, 912, 958, 979, 925, 920, ]
 ------------------------
 Threaded Gaussian Blur Cpp with radius 5
 (2073600, 325)
 avg: 325, min: 278, max: 384 ms, memoryUsed: 32
 times: [294, 330, 384, 290, 290, 294, 321, 351, 281, 316, 318, 314, 328, 278, 350, 337, 308, 349, 341, 296, 314, 343, 376, 357, 340, 370, 300, 328, 378, 283, ]
 ------------------------
 Gaussian Blur Rs with radius 5
 (2073600, 356)
 avg: 356, min: 304, max: 387 ms, memoryUsed: 46
 times: [337, 315, 385, 357, 358, 366, 345, 349, 379, 371, 304, 378, 353, 341, 384, 326, 380, 387, 341, 370, 328, 384, 365, 330, 373, 386, 362, 338, 345, 332, ]
------------------------
Gaussian Blur Intrinsic Rs with radius 5
(2073600, 49)
avg: 49, min: 33, max: 70 ms, memoryUsed: 23
times: [48, 61, 46, 56, 48, 49, 57, 49, 55, 62, 52, 51, 54, 38, 42, 44, 39, 33, 38, 36, 56, 70, 47, 40, 54, 54, 50, 49, 37, 43, ]
------------------------
Gaussian Blur OpenCv Cpp with radius 5
(2073600, 285)
avg: 285, min: 210, max: 372 ms, memoryUsed: 23
times: [275, 303, 210, 235, 232, 240, 251, 258, 250, 261, 215, 244, 273, 268, 329, 309, 283, 299, 281, 260, 311, 372, 351, 344, 367, 276, 288, 289, 321, 360, ]
 ------------------------
 Median Blur Cpp with radius 5
 (2073600, 1835)
 avg: 1835, min: 1779, max: 1909 ms, memoryUsed: 31
 times: [1897, 1819, 1852, 1801, 1813, 1831, 1909, 1805, 1868, 1846, 1807, 1779, 1872, 1846, 1855, 1820, 1845, 1818, 1789, 1879, 1823, 1798, 1817, 1800, 1819, 1896, 1844, 1864, 1829, 1818, ]
 ------------------------
 Median Blur Threaded Cpp with radius 5
 (2073600, 717)
 avg: 717, min: 650, max: 842 ms, memoryUsed: 44
 times: [661, 743, 675, 737, 675, 702, 728, 713, 709, 729, 701, 708, 784, 689, 738, 842, 715, 720, 695, 728, 747, 694, 739, 650, 713, 712, 734, 725, 724, 668, ]
 ------------------------
 Median Blur Java with radius 5
 (2073600, 2983)
 avg: 2983, min: 2851, max: 3180 ms, memoryUsed: 34
 times: [2922, 3007, 2946, 3145, 2948, 2907, 3026, 2887, 2995, 2979, 3180, 2906, 2851, 2938, 3053, 3017, 3098, 2997, 2862, 2969, 3142, 2894, 3099, 2890, 2941, 2896, 3078, 3015, 2934, 2981, ]
 ------------------------
 Median Blur Threaded Java with radius 5
 (2073600, 1230)
 avg: 1230, min: 1145, max: 1308 ms, memoryUsed: 48
 times: [1232, 1145, 1207, 1241, 1162, 1242, 1245, 1234, 1231, 1254, 1178, 1262, 1308, 1193, 1174, 1297, 1203, 1246, 1205, 1209, 1251, 1227, 1212, 1265, 1238, 1244, 1231, 1267, 1248, 1251, ]
 ------------------------
 Median Blur OpenCv Cpp with radius 5
 (2073600, 201)
 avg: 201, min: 122, max: 350 ms, memoryUsed: 23
 times: [122, 206, 186, 189, 180, 144, 264, 160, 202, 309, 157, 145, 137, 250, 146, 273, 169, 128, 192, 162, 162, 180, 215, 133, 204, 247, 210, 279, 337, 350, ]
 Difference between BoxBlurThreaded(4.84557037E8) and BoxBlur(4.84557037E8): 0.0, max diff: 0 at 0, 0
 Diff percentBoxBlurThreaded(0.0%) and BoxBlur(0.0)
 avgdiff : 0.0
 121, 99, 81
 121, 99, 81
 ------------------
 Difference between BoxBlurCpp(4.84557037E8) and BoxBlur(4.84557037E8): 0.0, max diff: 0 at 0, 0
 Diff percentBoxBlurCpp(0.0%) and BoxBlur(0.0)
 avgdiff : 0.0
 121, 99, 81
 121, 99, 81
 ------------------
 Difference between BoxBlurThreadedCpp(4.84557037E8) and BoxBlur(4.84557037E8): 0.0, max diff: 0 at 0, 0
 Diff percentBoxBlurThreadedCpp(0.0%) and BoxBlur(0.0)
 avgdiff : 0.0
 121, 99, 81
 121, 99, 81
 ------------------
 Difference between BoxBlurOpenCv(5.60757321E8) and BoxBlur(4.84557037E8): 8.0131504E7, max diff: 338 at 1431, 999
 Diff percentBoxBlurOpenCv(14.289872106725468%) and BoxBlur(16.5370633137663)
 avgdiff : 38.64366512345679
 198, 213, 223
 96, 89, 111
 ------------------
 Difference between BoxBlurRs(4.80463159E8) and BoxBlur(4.84557037E8): 2.0585842E7, max diff: 137 at 1655, 795
 Diff percentBoxBlurRs(4.284582826880177%) and BoxBlur(4.248383663448892)
 avgdiff : 9.927585841049384
 118, 112, 110
 163, 160, 154
 ------------------
 Difference between GaussianBlurThreaded(5.11826235E8) and GaussianBlur(5.11826235E8): 0.0, max diff: 0 at 0, 0
 Diff percentGaussianBlurThreaded(0.0%) and GaussianBlur(0.0)
 avgdiff : 0.0
 114, 90, 78
 114, 90, 78
 ------------------
 Difference between GaussianBlurCpp(5.11826235E8) and GaussianBlur(5.11826235E8): 0.0, max diff: 0 at 0, 0
 Diff percentGaussianBlurCpp(0.0%) and GaussianBlur(0.0)
 avgdiff : 0.0
 114, 90, 78
 114, 90, 78
 ------------------
 Difference between GaussianBlurThreadedCpp(5.11826235E8) and GaussianBlur(5.11826235E8): 0.0, max diff: 0 at 0, 0
 Diff percentGaussianBlurThreadedCpp(0.0%) and GaussianBlur(0.0)
 avgdiff : 0.0
 114, 90, 78
 114, 90, 78
 ------------------
 Difference between GaussianBlurRs(5.11838265E8) and GaussianBlur(5.11826235E8): 6848214.0, max diff: 69 at 1657, 813
 Diff percentGaussianBlurRs(1.3379644446864478%) and GaussianBlur(1.3379958922973145)
 avgdiff : 3.302572337962963
 168, 152, 144
 189, 176, 168
 ------------------
 Difference between GaussianBlurIntrinsicRs(5.60763135E8) and GaussianBlur(5.11826235E8): 5.3295138E7, max diff: 220 at 1659, 811
 Diff percentGaussianBlurIntrinsicRs(9.50403738648048%) and GaussianBlur(10.412740566141554)
 avgdiff : 25.701744791666666
 180, 175, 170
 109, 98, 98
 ------------------
 Difference between GaussianBlurOpenCv(5.60748475E8) and GaussianBlur(5.11826235E8): 5.0111942E7, max diff: 178 at 1163, 420
 Diff percentGaussianBlurOpenCv(8.936616724637549%) and GaussianBlur(9.790811524149403)
 avgdiff : 24.166638695987654
 0, 1, 4
 59, 60, 64
 ------------------
 Difference between MedianBlur(5.59399932E8) and MedianBlurCpp(5.59399932E8): 0.0, max diff: 0 at 0, 0
 Diff percentMedianBlur(0.0%) and MedianBlurCpp(0.0)
 avgdiff : 0.0
 126, 107, 92
 126, 107, 92
 ------------------
 Difference between MedianBlur(5.59399932E8) and MedianBlurThreadedCpp(5.59399932E8): 0.0, max diff: 0 at 0, 0
 Diff percentMedianBlur(0.0%) and MedianBlurThreadedCpp(0.0)
 avgdiff : 0.0
 126, 107, 92
 126, 107, 92
 ------------------
 Difference between MedianBlurThreaded(5.59399932E8) and MedianBlur(5.59399932E8): 0.0, max diff: 0 at 0, 0
 Diff percentMedianBlurThreaded(0.0%) and MedianBlur(0.0)
 avgdiff : 0.0
 126, 107, 92
 126, 107, 92
 ------------------
 Difference between MedianBlurOpenCv(5.59468633E8) and MedianBlur(5.59399932E8): 7075921.0, max diff: 213 at 1595, 809
 Diff percentMedianBlurOpenCv(1.264757411341772%) and MedianBlur(1.2649127386736971)
 avgdiff : 3.4123847415123456
 167, 112, 85
 167, 196, 214
 ------------------
 DONE
